'''
Created on Jun 25, 2014

@author: hauyeung
'''
import unittest, mysql.connector, addfood, tablepop
from database import login_info

class Test(unittest.TestCase):


    def setUp(self):
        self.db = mysql.connector.Connect(**login_info)
        self.cursor = self.db.cursor()
        self.cursor.execute("""DROP TABLE IF EXISTS food""")
        self.cursor.execute("""DROP TABLE IF EXISTS animal""")
        self.cursor.execute("""
            CREATE TABLE food (
                id INTEGER PRIMARY KEY AUTO_INCREMENT,
                anid INTEGER,
                feed VARCHAR(20),
                FOREIGN KEY (anid) REFERENCES animal(id))
            """)
        
        self.cursor.execute("""
            CREATE TABLE animal(
                id INTEGER PRIMARY KEY AUTO_INCREMENT,
                name VARCHAR(50),
                family VARCHAR(50),
                weight INTEGER);
            """)      
        tablepop.run()
        addfood.run()  
        

    def test_addfood(self):
        self.cursor.execute("insert into animal (name, family, weight) values('Jane','Chicken','40')")
        self.cursor.execute("select id from animal where name='Jane'")
        anid = self.cursor.fetchone()[0]
        self.cursor.execute("""insert into food (anid, feed) values (%s, %s)""",(anid, 'seeds'))
        self.cursor.execute('select count(name) from animal inner join food on (animal.id = food.anid) having count(feed) > 0')
        countfed = self.cursor.fetchone()
        self.assertGreater(countfed[0], 0)
    
        
    def test_havefood(self):
        self.cursor.execute('select count(name) from animal inner join food on (animal.id = food.anid) having count(feed) > 0')
        self.animalhavefood =  self.cursor.fetchone()[0]
        self.assertGreater(self.animalhavefood,0)
        
    def test_hungry(self):
        self.cursor.execute("insert into animal (name, family, weight) values('Joe','Dog','400')")
        self.cursor.execute("select id from animal where name='Joe'")
        anid = self.cursor.fetchone()[0]
        self.cursor.execute("select count(name) from animal left join food on (animal.id = food.anid) where feed is null")
        counthungry = self.cursor.fetchone()
        self.assertEqual(1, counthungry[0])
        self.cursor.execute("delete from animal where name='Joe'")
        self.cursor.execute("delete from food where anid = "+str(anid))
        
    def test_nofood(self):
        self.cursor.execute('select name from animal inner join food on (animal.id = food.anid) having count(feed) = 0')
        self.animalnofood =  self.cursor.fetchall()
        self.assertEqual([], self.animalnofood)
    
    def tearDown(self):     
        self.cursor.execute("""DROP TABLE IF EXISTS food""")
        self.cursor.execute("""DROP TABLE IF EXISTS animal""")
        self.db.close()


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()